import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {userActions} from '../_actions';

class UserOrdersPage extends React.Component {
    componentDidMount() {
        this.props.getMenuItems();
    }

    constructor(props) {
        super(props);

        this.state = {
            inventoryDetailsList: [{
                id: '',
                itemName: '',
                price: ''
            }],
            selectedOrders: []
        };

        //  this.handleSubmit = this.handleSubmit.bind(this);
    }

    orderSelected(id) {
        console.log('order placed id', id);
        console.log('order placed', this.props.menuItems.inventoryDetailsList.find(item => item.id == id));
        this.setState({
            selectedOrders:
                this.state.selectedOrders.concat(
                    this.props.menuItems.inventoryDetailsList
                        .find(item => item.id == id))
        });
    }

    render() {
        const {menuItems} = this.props;
        const selectedItems = this.state.selectedOrders;
        let totalBill =0;
        this.state.selectedOrders.forEach(value => totalBill = totalBill + value.price)
        console.log('bill', totalBill);
        return (

            <div className="col-md-8 col-md-offset-3">
                <h2>Order Catalog</h2>
                <p>
                    <Link to="/userLogin">Logout</Link>
                </p>
                <h3 className="text-secondary"> Select the Items to place order</h3>
                {menuItems &&
                <ul>
                    {menuItems.inventoryDetailsList.map((user, index) =>
                        <li key={user.id} className="w-100 m-4 font-weight-bold">
                            {user.itemName + ' - price : ' + user.price}
                            <button type="button" className="btn btn-sm btn-primary float-right"
                                    onClick={() => {
                                        this.orderSelected(user.id)
                                    }}>Select
                            </button>
                        </li>
                    )}
                </ul>
                }
                {this.state?.selectedOrders.length > 0 && <h3>selected items</h3>}
                {this.state?.selectedOrders &&
                <ul>
                    {this.state?.selectedOrders?.map((user, index) =>
                        <li key={user.id} className="w-100 m-4 font-weight-bold">
                            {user.itemName + ' ' + user.price}
                        </li>
                    )}
                </ul>}
                {totalBill > 0 && <h3>Total bill : {totalBill}</h3>}
            </div>
        );
    }
}

function mapState(state) {
    const {menuItems} = state.menuItems;
    return {menuItems};
}

const actionCreators = {
    getMenuItems: userActions.getMenuItems
}

const connectedRegisterPage = connect(mapState, actionCreators)(UserOrdersPage);
export {connectedRegisterPage as UserOrdersPage};