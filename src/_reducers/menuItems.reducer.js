import {userConstants} from "../_constants";

export function menuItems(state = {}, action) {
    switch (action.type) {
        case userConstants.GET_MENU_ITEMS:
            return {
                loading: true
            };
        case userConstants.GET_MENU_ITEMS_SUCCESS:
            return {
                menuItems: action.menuItems
            };
        default:
            return state
    }
}