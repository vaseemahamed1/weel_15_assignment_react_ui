import React from 'react';
import {Router, Route, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import {history} from '../_helpers';
import {alertActions} from '../_actions';
import {PrivateOrderRoute, PrivateRoute} from '../_components';
import {AdminPage} from '../AdminPage';
import {UserLoginPage} from '../UserLoginPage';
import {RegisterPage} from '../RegisterPage';
import {UserOrdersPage} from "../UserOrdersPage";

class App extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });
    }

    render() {
        const {alert} = this.props;
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <a className="navbar-brand" href="/userLogin">USER</a>
                    <a className="navbar-brand" href="/userLogin">ADMIN</a>
                </nav>
                <div className="jumbotron">

                    <div className="container">
                        <div className="col-sm-8 col-sm-offset-2">
                            {alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
                            }
                            <Router history={history}>
                                <Switch>
                                    <PrivateRoute exact path="/" component={UserLoginPage}/>
                                    <PrivateRoute exact path="/adminPortal" component={AdminPage}/>
                                    <Route exact path="/userLogin" component={UserLoginPage}/>
                                    <Route exact path="/userRegister" component={RegisterPage}/>
                                    <PrivateOrderRoute exact path="/userOrders" component={UserOrdersPage}/>
                                    <Redirect from="*" to="/"/>
                                </Switch>
                            </Router>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const {alert} = state;
    return {alert};
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export {connectedApp as App};