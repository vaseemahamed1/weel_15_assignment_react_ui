This project is the Front end application build on react framework.

Steps to run locally:

- Clone https://gitlab.com/vaseemahamed1/week_15_assignment_backend,  run it as spring boot application to setup the backend apis locally.
- Clone the current repository and run below commands
    - npm install
    - npm start
- Application should be up and running on http://localhost:8080
